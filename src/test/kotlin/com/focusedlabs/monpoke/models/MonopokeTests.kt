package com.focusedlabs.monpoke.models

import com.focusedlabs.monpoke.models.Monopoke
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class MonopokeTests {
    @Test
    fun `Validate creation`() {
        assertThrows<Exception> { Monopoke("ZeroHp", 0, 1) }
        assertThrows<Exception> { Monopoke("ZeroAp", 1, 0) }
    }
}