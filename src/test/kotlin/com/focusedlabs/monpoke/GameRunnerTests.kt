package com.focusedlabs.monpoke

import com.focusedlabs.monpoke.gamecommands.GameCommand
import com.focusedlabs.monpoke.gamecommands.impl.AttackCommand
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class GameRunnerTests {
    private lateinit var createCommand: GameCommand
    private lateinit var iChooseYouCommand: GameCommand
    private lateinit var attackCommand: GameCommand
    private lateinit var gameRunner: GameRunner

    @BeforeEach
    fun setup() {
        createCommand = mock()
        iChooseYouCommand = mock()
        attackCommand = mock()
        gameRunner = GameRunner(createCommand, iChooseYouCommand, attackCommand)
    }

    @Test
    fun `Delegates create command`() {
        val command = "CREATE MyTeam MyMonster 1 1"

        gameRunner.performCommand(command)

        verify(createCommand).execute(command.split(" "))
    }

    @Test
    fun `Delegates I choose you command`() {
        val command = "ICHOOSEYOU MyMonster"

        gameRunner.performCommand(command)

        verify(iChooseYouCommand).execute(command.split(" "))
    }

    @Test
    fun `Delegates I attack command`() {
        val command = "ATTACK"

        gameRunner.performCommand(command)

        verify(attackCommand).execute(command.split(" "))
    }
}