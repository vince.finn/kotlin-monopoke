package com.focusedlabs.monpoke

import com.focusedlabs.monpoke.gamecommands.impl.AttackCommand
import com.focusedlabs.monpoke.gamecommands.impl.CreateCommand
import com.focusedlabs.monpoke.gamecommands.impl.IChooseYouCommand
import com.focusedlabs.monpoke.models.GameState
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MonopokeEndToEndTests {

    private lateinit var fakeLogCommand: FakeLogCommand
    private lateinit var gameRunner: GameRunner

    @BeforeEach
    fun setup() {
        fakeLogCommand = FakeLogCommand()
        val gameState = GameState()
        gameRunner = GameRunner(
            CreateCommand(gameState, fakeLogCommand),
            IChooseYouCommand(gameState, fakeLogCommand),
            AttackCommand(gameState, fakeLogCommand),
        )
    }

    @Test
    fun `Standard game battle logs`() {
        gameRunner.performCommand("CREATE Rocket Meekachu 3 1")
        gameRunner.performCommand("CREATE Rocket Rastly 5 6")
        gameRunner.performCommand("CREATE Green Smorelax 2 1")
        gameRunner.performCommand("ICHOOSEYOU Meekachu")
        gameRunner.performCommand("ICHOOSEYOU Smorelax")
        gameRunner.performCommand("ATTACK")
        gameRunner.performCommand("ATTACK")
        gameRunner.performCommand("ICHOOSEYOU Rastly")
        gameRunner.performCommand("ATTACK")
        gameRunner.performCommand("ATTACK")

        assertLogs(listOf(
            "Meekachu has been assigned to team Rocket!",
            "Rastly has been assigned to team Rocket!",
            "Smorelax has been assigned to team Green!",
            "Meekachu has entered the battle!",
            "Smorelax has entered the battle!",
            "Meekachu attacked Smorelax for 1 damage!",
            "Smorelax attacked Meekachu for 1 damage!",
            "Rastly has entered the battle!",
            "Smorelax attacked Rastly for 1 damage!",
            "Rastly attacked Smorelax for 6 damage!",
            "Smorelax has been defeated!",
            "Rocket is the winner!"
        ))
    }

    private fun assertLogs(expectedOutput: List<String>
    ) {
        for (i in 0..expectedOutput.lastIndex) {
            assertThat(fakeLogCommand.logs[i])
                .`as`("Assert log #${i + 1}")
                .isEqualTo(expectedOutput[i])
        }
    }

    private class FakeLogCommand : LogCommand() {
        var logs = mutableListOf<String>()

        override fun execute(message: String) {
            logs.add(message)
        }
    }
}