package com.focusedlabs.monpoke.gamecommands.impl

import com.focusedlabs.monpoke.models.GameState
import com.focusedlabs.monpoke.LogCommand
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

class AttackCommandTests {
    private lateinit var gameState: GameState
    private lateinit var logCommand: LogCommand
    private lateinit var createCommand: CreateCommand
    private lateinit var iChooseYouCommand: IChooseYouCommand
    private lateinit var attackCommand: AttackCommand

    @BeforeEach
    fun setup() {
        gameState = GameState()
        logCommand = mock()
        createCommand = CreateCommand(gameState, logCommand)
        iChooseYouCommand = IChooseYouCommand(gameState, logCommand)
        attackCommand = AttackCommand(gameState, logCommand)
    }

    @Test
    fun `Logs message`() {
        createCommand.execute("CREATE Team1 Dog 5 3".split(" "))
        createCommand.execute("CREATE Team2 Cat 5 2".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Dog".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Cat".split(" "))

        attackCommand.execute("ATTACK".split(" "))
        verify(logCommand).execute("Dog attacked Cat for 3 damage!")

        attackCommand.execute("ATTACK".split(" "))
        verify(logCommand).execute("Cat attacked Dog for 2 damage!")

        attackCommand.execute("ATTACK".split(" "))
        verify(logCommand, times(2)).execute("Dog attacked Cat for 3 damage!")
        verify(logCommand).execute("Cat has been defeated!")
        verify(logCommand).execute("Team1 is the winner!")
    }

    @Test
    fun `Players take turns attacking`() {
        createCommand.execute("CREATE Team1 Dog 5 1".split(" "))
        createCommand.execute("CREATE Team2 Cat 5 2".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Dog".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Cat".split(" "))
        val team1 = gameState.getTeam("Team1")
        val team2 = gameState.getTeam("Team2")

        attackCommand.execute("ATTACK".split(" "))
        assertThat(team2.getMonopoke("Cat").hp).isEqualTo(4)

        attackCommand.execute("ATTACK".split(" "))
        assertThat(team1.getMonopoke("Dog").hp).isEqualTo(3)
    }

    @Test
    fun `Cannot attack if monopoke is defeated`() {
        createCommand.execute("CREATE Team1 Mouse 1 1".split(" "))
        createCommand.execute("CREATE Team1 Bird 1 1".split(" "))
        createCommand.execute("CREATE Team2 Cat 5 2".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Mouse".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Cat".split(" "))

        attackCommand.execute("ATTACK".split(" "))
        attackCommand.execute("ATTACK".split(" "))

        assertThrows<Exception> { attackCommand.execute("ATTACK".split(" ")) }
    }

    @Test
    fun `Ends game when all of one team's monopoke are defeated`() {
        createCommand.execute("CREATE Team1 Mouse 1 1".split(" "))
        createCommand.execute("CREATE Team1 Bird 1 1".split(" "))
        createCommand.execute("CREATE Team2 Cat 5 2".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Mouse".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Cat".split(" "))

        attackCommand.execute("ATTACK".split(" "))
        attackCommand.execute("ATTACK".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Bird".split(" "))
        attackCommand.execute("ATTACK".split(" "))

        assertThat(gameState.isGameOver).isTrue
    }
}