package com.focusedlabs.monpoke.gamecommands.impl

import com.focusedlabs.monpoke.models.GameState
import com.focusedlabs.monpoke.LogCommand
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import java.lang.Exception

class IChooseYouCommandTests {
    private lateinit var gameState: GameState
    private lateinit var logCommand: LogCommand
    private lateinit var createCommand: CreateCommand
    private lateinit var attackCommand: AttackCommand
    private lateinit var iChooseYouCommand: IChooseYouCommand

    @BeforeEach
    fun setup() {
        gameState = GameState()
        logCommand = mock()
        createCommand = CreateCommand(gameState, logCommand)
        attackCommand = AttackCommand(gameState, logCommand)
        iChooseYouCommand = IChooseYouCommand(gameState, logCommand)
    }

    @Test
    fun `Logs message`() {
        createCommand.execute("CREATE Team1 Poke1 1 1".split(" "))
        createCommand.execute("CREATE Team2 Poke2 1 1".split(" "))

        iChooseYouCommand.execute("ICHOOSEYOU Poke1".split(" "))
        verify(logCommand).execute("Poke1 has entered the battle!")

        iChooseYouCommand.execute("ICHOOSEYOU Poke2".split(" "))
        verify(logCommand).execute("Poke2 has entered the battle!")
    }

    @Test
    fun `Players take turns choosing monopoke`() {
        createCommand.execute("CREATE Team1 Poke1 1 1".split(" "))
        createCommand.execute("CREATE Team2 Poke2 1 1".split(" "))
        val team1 = gameState.getTeam("Team1")
        val team2 = gameState.getTeam("Team2")

        iChooseYouCommand.execute("ICHOOSEYOU Poke1".split(" "))
        assertThat(team1.chosenMonopoke.name).isEqualTo("Poke1")

        iChooseYouCommand.execute("ICHOOSEYOU Poke2".split(" "))
        assertThat(team2.chosenMonopoke.name).isEqualTo("Poke2")
    }

    @Test
    fun `Cannot choose a defeated monopoke`() {
        createCommand.execute("CREATE Team1 Mouse 1 1".split(" "))
        createCommand.execute("CREATE Team1 Bird 1 1".split(" "))
        createCommand.execute("CREATE Team2 Cat 5 2".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Mouse".split(" "))
        iChooseYouCommand.execute("ICHOOSEYOU Cat".split(" "))
        attackCommand.execute("ATTACK".split(" "))
        attackCommand.execute("ATTACK".split(" "))

        assertThrows<Exception> { iChooseYouCommand.execute("ICHOOSEYOU Mouse".split(" ")) }
    }
}