package com.focusedlabs.monpoke.gamecommands.impl

import com.focusedlabs.monpoke.models.GameState
import com.focusedlabs.monpoke.LogCommand
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import java.lang.Exception

class CreateCommandTests {
    private lateinit var gameState: GameState
    private lateinit var logCommand: LogCommand
    private lateinit var createCommand: CreateCommand
    private lateinit var iChooseYouCommand: IChooseYouCommand

    @BeforeEach
    fun setup() {
        gameState = GameState()
        logCommand = mock()
        iChooseYouCommand = IChooseYouCommand(gameState, logCommand)
        createCommand = CreateCommand(gameState, logCommand)
    }

    @Test
    fun `Logs message`() {
        createCommand.execute("CREATE Red Dog 1 1".split(" "))
        verify(logCommand).execute("Dog has been assigned to team Red!")

        createCommand.execute("CREATE Blue Cat 1 1".split(" "))
        verify(logCommand).execute("Cat has been assigned to team Blue!")
    }

    @Test
    fun `Adds teams with monopoke to game state`() {
        createCommand.execute("CREATE Pink Pig 1 2".split(" "))

        val pinkPig = gameState.getTeam("Pink").getMonopoke("Pig")
        assertThat(pinkPig.name).isEqualTo("Pig")
        assertThat(pinkPig.hp).isEqualTo(1)
        assertThat(pinkPig.ap).isEqualTo(2)

        createCommand.execute("CREATE Yellow Bee 3 4".split(" "))
        val yellowBee = gameState.getTeam("Yellow").getMonopoke("Bee")
        assertThat(yellowBee.name).isEqualTo("Bee")
        assertThat(yellowBee.hp).isEqualTo(3)
        assertThat(yellowBee.ap).isEqualTo(4)
    }

    @Test
    fun `After adding a team once, subsequently add monopoke to that team`() {
        createCommand.execute("CREATE Gray Mouse 1 1".split(" "))
        createCommand.execute("CREATE Gray Rat 1 1".split(" "))

        val grayTeam = gameState.getTeam("Gray")
        assertThat(gameState.teamCount).isEqualTo(1)
        assertThat(grayTeam.getMonopoke("Mouse")).isNotNull
        assertThat(grayTeam.getMonopoke("Rat")).isNotNull
    }

    @Test
    fun `No more than two teams are allowed`() {
        createCommand.execute("CREATE Team1 Monster 1 1".split(" "))
        createCommand.execute("CREATE Team2 Monster 1 1".split(" "))
        assertThrows<Exception> { createCommand.execute("CREATE Team3 Monster 1 1".split(" ")) }
    }

    @Test
    fun `Cannot add two monopoke with the same name to one team`() {
        createCommand.execute("CREATE MyTeam Mimic 1 1".split(" "))
        assertThrows<Exception> { createCommand.execute("CREATE MyTeam Mimic 1 1".split(" ")) }
    }

    @Test
    fun `Starts game, and cannot call create command`() {
        createCommand.execute("CREATE Team1 Poke1 1 1".split(" "))
        createCommand.execute("CREATE Team2 Poke2 1 1".split(" "))

        iChooseYouCommand.execute("ICHOOSEYOU Poke1".split(" "))

        assertThrows<Exception> { createCommand.execute("CREATE Team1 Poke1 1 1".split(" ")) }
    }
}