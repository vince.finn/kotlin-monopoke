package com.focusedlabs.monpoke.gamecommands

import java.lang.reflect.Executable

interface GameCommand{
    fun execute(commandParams: List<String>)
}