package com.focusedlabs.monpoke.gamecommands.impl

import com.focusedlabs.monpoke.models.GameState
import com.focusedlabs.monpoke.LogCommand
import com.focusedlabs.monpoke.gamecommands.GameCommand
import java.lang.Exception

class AttackCommand(
    private val gameState: GameState,
    private val logCommand: LogCommand,
) : GameCommand
{
    override fun execute(commandParams: List<String>) {
        val chosenMonopoke = gameState.currentPlayer.chosenMonopoke
        val monopokeToAttack = gameState.opponent.chosenMonopoke

        if(chosenMonopoke.isDefeated) throw Exception("Cannot attack with a defeated monopoke")
        monopokeToAttack.receiveDamage(chosenMonopoke.ap)
        if(!gameState.isGameOver) gameState.nextTurn()

        logCommand.execute("${chosenMonopoke.name} attacked ${monopokeToAttack.name} for ${chosenMonopoke.ap} damage!")
        if(monopokeToAttack.isDefeated) logCommand.execute("${monopokeToAttack.name} has been defeated!")
        if(gameState.isGameOver) logCommand.execute("${gameState.currentPlayer.name} is the winner!")
    }
}