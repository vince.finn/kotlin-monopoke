package com.focusedlabs.monpoke.gamecommands.impl

import com.focusedlabs.monpoke.models.GameState
import com.focusedlabs.monpoke.LogCommand
import com.focusedlabs.monpoke.gamecommands.GameCommand

class IChooseYouCommand(
    private val gameState: GameState,
    private val logCommand: LogCommand,
) : GameCommand {
    override fun execute(commandParams: List<String>) {
        val name = commandParams[1]
        if(!gameState.isStarted) gameState.startGame()
        gameState.currentPlayer.choseMonopoke(name)
        gameState.nextTurn()
        logCommand.execute("$name has entered the battle!")
    }
}