package com.focusedlabs.monpoke.gamecommands.impl

import com.focusedlabs.monpoke.models.GameState
import com.focusedlabs.monpoke.LogCommand
import com.focusedlabs.monpoke.models.Monopoke
import com.focusedlabs.monpoke.models.Team
import com.focusedlabs.monpoke.gamecommands.GameCommand
import java.lang.Exception

class CreateCommand(
    private val gameState: GameState,
    private val logCommand: LogCommand,
    ) : GameCommand
{
    override fun execute(commandParams: List<String>) {
        if(gameState.isStarted) throw Exception("Cannot add more monopoke after the game has started")

        val teamName = commandParams[1]
        val monopokeName = commandParams[2]
        val monopokeHp = Integer.parseInt(commandParams[3])
        val monopokeAp = Integer.parseInt(commandParams[4])

        gameState.addTeamIfNew(Team(teamName))
        val team = gameState.getTeam(teamName)
        team.addMonopoke(Monopoke(monopokeName, monopokeHp, monopokeAp))

        logCommand.execute("$monopokeName has been assigned to team $teamName!")
    }
}