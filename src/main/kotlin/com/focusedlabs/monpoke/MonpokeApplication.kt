package com.focusedlabs.monpoke

import com.focusedlabs.monpoke.gamecommands.impl.AttackCommand
import com.focusedlabs.monpoke.gamecommands.impl.CreateCommand
import com.focusedlabs.monpoke.gamecommands.impl.IChooseYouCommand
import com.focusedlabs.monpoke.models.GameState
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MonpokeApplication : CommandLineRunner {

	override fun run(vararg args: String?) {
		val gameState = GameState()
		val gameRunner = GameRunner(
			CreateCommand(gameState, LogCommand()),
			IChooseYouCommand(gameState, LogCommand()),
			AttackCommand(gameState, LogCommand())
		)
		
		while (!gameState.isGameOver) {
			val line = readLine()
			if (line.isNullOrEmpty()) break

			gameRunner.performCommand(line)
		}
	}

}

fun main(args: Array<String>){
	runApplication<MonpokeApplication>(*args)
}