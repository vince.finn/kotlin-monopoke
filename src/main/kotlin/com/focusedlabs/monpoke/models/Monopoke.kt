package com.focusedlabs.monpoke.models

import java.lang.Exception

class Monopoke(
    val name: String,
    var hp: Int,
    val ap: Int,
) {
    var isDefeated = false
        private set

    init {
        if(hp < 1) throw Exception("Must have 1 or more hp")
        if(ap < 1) throw Exception("Must have 1 or more ap")
    }

    fun receiveDamage(damageToDeal: Int) {
        hp -= damageToDeal
        if(hp <= 0) isDefeated = true
    }
}
