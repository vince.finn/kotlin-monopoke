package com.focusedlabs.monpoke.models

import java.lang.Exception

class Team(val name: String) {
    val isDefeated: Boolean get() = monopokes.all { it.isDefeated }
    lateinit var chosenMonopoke: Monopoke
        private set
    private val monopokes = mutableListOf<Monopoke>()

    fun getMonopoke(name: String): Monopoke = monopokes.find { it.name == name } ?: throw NullPointerException("Unable to find monopoke with name: $name")

    fun addMonopoke(monopoke: Monopoke) {
        if(monopokes.find{ it.name == monopoke.name} != null) throw Exception("Cannot add multiple monopoke with the same name to a team")
        monopokes.add(monopoke)
    }

    fun choseMonopoke(name: String) {
        val monopokeToChoose = getMonopoke(name)
        if(monopokeToChoose.isDefeated) throw Exception("Cannot choose defeated monopoke")
        chosenMonopoke = monopokeToChoose
    }

    companion object {
        fun default(): Team {
            return Team("")
        }
    }
}
