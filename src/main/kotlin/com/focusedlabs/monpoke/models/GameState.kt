package com.focusedlabs.monpoke.models
import java.lang.Exception

class GameState {
    val teamCount get() = teams.size
    lateinit var currentPlayer: Team
    lateinit var opponent: Team
    var isStarted = false
        private set
    val isGameOver: Boolean get() {
        return isStarted && (currentPlayer.isDefeated || opponent.isDefeated)
    }

    private val teams = mutableListOf<Team>()

    fun getTeam(teamName: String): Team = teams.find { it.name == teamName } ?: throw NullPointerException("Unable to find team with name: $teamName")

    fun addTeamIfNew(team: Team) {
        if(teams.find{ it.name == team.name} != null) return
        teams.add(team)
        if (teamCount > 2) throw Exception("No more than two teams allowed")
    }

    fun startGame() {
        isStarted = true
        currentPlayer = teams[0]
        opponent = teams[1]
    }

    fun nextTurn() {
        currentPlayer = opponent.also { opponent = currentPlayer }
    }
}