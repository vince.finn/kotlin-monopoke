package com.focusedlabs.monpoke

import com.focusedlabs.monpoke.gamecommands.GameCommand

class GameRunner(
    private val createCommand: GameCommand,
    private val iChooseYouCommand: GameCommand,
    private val attackCommand: GameCommand,
) {
    fun performCommand(command: String) {
        val commandParams = command.split(" ")
        when(commandParams[0]) {
            "CREATE" -> createCommand.execute(commandParams)
            "ICHOOSEYOU" -> iChooseYouCommand.execute(commandParams)
            "ATTACK" -> attackCommand.execute(commandParams)
        }
    }
}
