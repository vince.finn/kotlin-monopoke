package com.focusedlabs.monpoke

open class LogCommand {
    open fun execute(message: String) {
        println(message)
    }
}